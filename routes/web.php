<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'admin'], function () {

	Auth::routes();
	
	Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function() {
		Route::get('/', 'AdminController@index');
		Route::resource('noticias', 'NoticiasController');
		Route::resource('correlativities', 'CorrelativitiesController');
		Route::resource('examDates', 'ExamDatesController');
		Route::resource('schedules', 'SchedulesController');
		Route::resource('resumes', 'ResumesController');
	});

});

Route::get('/home', 'HomeController@index')->name('home');
