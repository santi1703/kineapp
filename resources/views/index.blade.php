@extends('layouts.front')

@section('content')

<div class="container-fluid">
	<div class="row">
		<ul class="nav nav-tabs main">
			<li class="active"><a data-toggle="tab" href="#home-front">Noticias</a></li>
			<li><a data-toggle="tab" href="#institutional">Institucional</a></li>
			<li><a data-toggle="tab" href="#links">Vínculos de interés</a></li>
		</ul>

		<div class="tab-content">
			<div id="home-front" class="tab-pane fade in active">
				<div class="col-xs-12">
					@include('partials.home_panel')
				</div>
			</div>
			<div id="institutional" class="tab-pane fade">
				<div class="col-xs-12">
					@include('partials.institutional_panel')
				</div>
			</div>
			<div id="links" class="tab-pane fade">
				<div class="col-xs-12">
					@include('partials.links_panel')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection