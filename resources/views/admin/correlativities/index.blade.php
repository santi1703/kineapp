@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped">
			<tr>
				<th>Año</th>
				<th>Imagen</th>
				<th>Posición</th>
				<th>Acciones</th>
			</tr>	
			@forelse ($correlativities as $correlativity)
			<td>{{ $correlativity->name }}</td>
			<td></td>
			<td width="10%">{{ $correlativity->position }}</td>
			<td width="20%">
				<div class="row">
					<div class="col-md-6">
						<a href="{{ route('correlativities.edit', [$correlativity->id]) }}" class="btn btn-success">Editar</a>
					</div>
					<div class="col-md-6">
						{{ Form::open(['class' => 'delete', 'action' => ['CorrelativitiesController@destroy',$correlativity->id], 'method' => 'DELETE']) }}
						{{ Form::submit('Eliminar', ['class' => 'btn btn-danger btn-delete']) }}
						{{ Form::close() }}
					</div>
				</div>
			</td>		
			@empty
			<tr>
				<td>No hay actualmente correlatividades cargadas</td>
			</tr>
			@endforelse
		</table>
	</div>
	<div class="col-md-12 text-center">
		<a href="{{ action('CorrelativitiesController@create') }}" class="btn btn-primary">Nueva correlatividad</a>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$('.btn-delete').click(function(){
		confirm('¿Va a eliminar el elemento, está seguro?');
	})

	$('.delete').submit(function(e){

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('[name^="_token"]').val()
			}
		});

		form = $(this);
		action = form.attr('action');
		
		e.preventDefault();

		$.when($.ajax({
			url: action,
			type: 'delete',
		})).done(function(data){
			res = JSON.parse(data);

			iziToast.show({
				title: res.message,
				class: 'iziToast-color-green',
				timeout: 5000,
				icon: 'fa fa-check',
			});

			form.closest('tr').fadeOut(50);
		})

	});
</script>
@endsection