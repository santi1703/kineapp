@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		{{ Form::model($resume, ['url' => route('resumes.update', [$resume->id]), 'files' => true, 'method' => 'patch']) }}

		<div class="form-group">
			{{ Form::label('name', 'Año', ['class' => 'control-label']) }}
			{{ Form::text('name', $resume->name, ['class' => 'form-control', 'maxlength' => 255]) }}
		</div>

		<div class="form-group">
			{{ Form::label('position', 'Posición', ['class' => 'control-label']) }}
			{{ Form::selectRange('position', 1, App\Models\Resume::all()->count(), $resume->position, ['class' => 'form-control']) }}
		</div>

		{{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
	</div>
</div>
@endsection