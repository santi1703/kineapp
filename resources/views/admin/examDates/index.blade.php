@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped">
			<tr>
				<th>Año</th>
				<th>Imagen</th>
				<th>Acciones</th>
			</tr>	
			@forelse ($examDates as $examDate)
			<tr>
				<td>{{ $examDate->name }}</td>
				<td></td>
				<td>
					{{ Form::open(['class' => 'delete', 'action' => ['ExamDatesController@destroy',$examDate->id], 'method' => 'DELETE']) }}
					{{ Form::submit('Eliminar', ['class' => 'btn btn-danger btn-delete']) }}
					{{ Form::close() }}
				</td>
			</tr>		
			@empty
			<tr>
				<td>No hay actualmente fechas de exámen cargadas</td>
			</tr>
			@endforelse
		</table>
	</div>
	<div class="col-md-12 text-center">
		<a href="{{ action('ExamDatesController@create') }}" class="btn btn-primary">Nueva fecha de exámen</a>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$('.btn-delete').click(function(){
		confirm('¿Va a eliminar el elemento, está seguro?');
	})

	$('.delete').submit(function(e){

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('[name^="_token"]').val()
			}
		});

		form = $(this);
		action = form.attr('action');
		
		e.preventDefault();

		$.when($.ajax({
			url: action,
			type: 'delete',
		})).done(function(data){
			res = JSON.parse(data);

			iziToast.show({
				title: res.message,
				class: 'iziToast-color-green',
				timeout: 5000,
				icon: 'fa fa-check',
			});

			form.closest('tr').fadeOut(50);
		})

	});
</script>
@endsection