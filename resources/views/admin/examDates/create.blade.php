@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		{{ Form::open(['url' => 'admin/examDates', 'files' => true]) }}

		<div class="form-group">
			{{ Form::label('name', 'Año', ['class' => 'control-label']) }}
			{{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => 255]) }}
		</div>

		<div class="form-group">
			{{ Form::label('image', 'Imagen', ['class' => 'control-label']) }}
			{{ Form::file('image', null, ['class' => 'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('position', 'Posición', ['class' => 'control-label']) }}
			{{ Form::select('position', selectRangeFirstValue(App\Models\ExamDate::all()->count()+1, 'Seleccione una'), null, ['class' => 'form-control']) }}
		</div>

		{{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
	</div>
</div>
@endsection