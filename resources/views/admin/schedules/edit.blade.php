@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		{{ Form::model($schedule, ['url' => route('schedules.update', [$schedule->id]), 'files' => true, 'method' => 'patch']) }}

		<div class="form-group">
			{{ Form::label('name', 'Año', ['class' => 'control-label']) }}
			{{ Form::text('name', $schedule->name, ['class' => 'form-control', 'maxlength' => 255]) }}
		</div>

		<div class="form-group">
			{{ Form::label('position', 'Posición', ['class' => 'control-label']) }}
			{{ Form::selectRange('position', 1, App\Models\Schedule::all()->count(), $schedule->position, ['class' => 'form-control']) }}
		</div>

		{{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
	</div>
</div>
@endsection