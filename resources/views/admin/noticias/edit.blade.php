@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		{{ Form::model($noticia, ['url' => route('noticias.update', [$noticia->id]), 'files' => true, 'method' => 'patch']) }}

		<div class="form-group">
			{{ Form::label('name', 'Año', ['class' => 'control-label']) }}
			{{ Form::text('name', $noticia->name, ['class' => 'form-control', 'maxlength' => 255]) }}
		</div>

		<div class="form-group">
			{{ Form::label('position', 'Posición', ['class' => 'control-label']) }}
			{{ Form::selectRange('position', 1, App\Models\Noticia::all()->count(), $noticia->position, ['class' => 'form-control']) }}
		</div>

		{{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
	</div>
</div>
@endsection