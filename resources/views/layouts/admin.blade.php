<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ Config::get('app.name') }}</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('iziToast/css/iziToast.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
</head>
<body>	
	<div class="row">
		<div class="col-md-12">
			<h1>{{ Config::get('app.name') }} Admin</h1>
		</div>
	</div>
	<div class="row">
		@if(Auth::check())
		<div class="col-md-2">
			<div class="list-group">
				<a class="list-group-item" href="{{ url('admin/resumes') }}">Descripción</a>
				<a class="list-group-item" href="{{ url('admin/noticias') }}">Noticias</a>
				<a class="list-group-item" href="{{ url('admin/schedules') }}">Horarios de cursado</a>
				<a class="list-group-item" href="{{ url('admin/examDates') }}">Fechas de examen</a>
				<a class="list-group-item" href="{{ url('admin/correlativities') }}">Correlatividades</a>
				{{ Form::open(['url' => route('logout')]) }}
				{{ Form::submit('Salir', ['class' => 'btn btn-primary']) }}
				{{ Form::close() }}
			</div>
		</div>
		<div class="col-md-10">
		@else
		<div class="col-md-12">
		@endif
			<div class="container">
				<div class="jumbotron text-center">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="{{ asset('jquery/jquery-3.2.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('iziToast/js/iziToast.min.js') }}"></script>
@if (Session::has('message'))
<script>
	iziToast.show({
		title: '{{ Session::get('message') }}',
		class: 'iziToast-color-green',
		timeout: 5000,
		icon: 'fa fa-check',
	});
</script>
@endif
@if (Session::has('errors'))
@foreach( Session::get('errors')->all() as $error)
<script>
	iziToast.show({
		title: '{{ $error }}',
		class: 'iziToast-color-red',
		timeout: false,
		icon: 'fa fa-warning',
	});
</script>
@endforeach
@endif
@yield('scripts')
</html>