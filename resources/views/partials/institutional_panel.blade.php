<div class="main-title">Institucional</div>

<ul class="nav nav-tabs" id="tab-panel-institutional" hidden>
	
</ul>

<div class="tab-content" id="tab-content-institutional" hidden>
	
</div>

<div id="content-institutional">
	<div>
		<div class="list-group links-list">

			<a 
			href="#"
			class="list-group-item open-tab" 
			data-name="link-guarani" 
			data-title="Guaraní" 
			data-href="https://guarani.unc.edu.ar/kinesiologia/"><img src="{{ asset('img/guarani.png') }}">Guaraní</a>

			<a 
			href="nav://ekyf.proed.unc.edu.ar/" 
			class="list-group-item open-tab ext-link" 
			name="link-aula-virtual" 
			title="Aula virtual"><img src="{{ asset('img/aulavirtual.png') }}">Aula virtual</a>

		</div>
	</div>
	<div class="panel-group" id="accordion">
		<div class="panel panel-primary">
			<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
				<div class="title panel-title">
					<a>
						Horarios de cursado
					</a>
				</div>
			</div>
			<div id="collapse1" class="panel-collapse collapse">
				<div class="list-group">
					@forelse ($schedules as $schedule)
					<a class="list-group-item" 
					href="#" 
					data-title="{{ $schedule->name }}"
					data-name="{{ str_slug($schedule->name) }}" 
					data-img="{{ asset('upload/img').'/'.$schedule->image()->first()->filename }}">{{ $schedule->name }}</a>
					@empty
					Aún no hay horarios de cursado registrados
					@endforelse
				</div>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
				<div class="title panel-title">
					<a>
						Fechas de exámen
					</a>
				</div>
			</div>
			<div id="collapse2" class="panel-collapse collapse">
				<div class="list-group">
					@forelse ($examDates as $examDate)
					<a class="list-group-item" 
					href="#" 
					data-title="{{ $examDate->name }}"
					data-name="{{ str_slug($examDate->name) }}" 
					data-img="{{ asset('upload/img').'/'.$examDate->image()->first()->filename }}">{{ $examDate->name }}</a>
					@empty
					Aún no hay fechas de exámen registradas
					@endforelse
				</div>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
				<div class="title panel-title">
					<a>
						Correlatividades
					</a>
				</div>
			</div>
			<div id="collapse3" class="panel-collapse collapse">
				@if (!is_null($correlativity))
				<div class="img-container">
					<img src="{{ asset('upload/img/'.$correlativity->image()->first()->filename) }}">
				</div>
				@else
				Aún no hay correlatividades registradas
				@endif
			</div>
		</div>
	</div>
</div>


@section('scripts')
@parent

<script>

	function initializeLinksTabInst() {
		$('#content-institutional .list-group-item').not('.ext-link').click( function(){ 			
			newTabInst($(this).data()) 
		});
		$('#tab-content-institutional .list-group-item').not('.ext-link').click( function(){ 			
			newTabInst($(this).data()) 
		});
	}

	initializeLinksTabInst();

	function newTabInst(data) {

		ids = $('#tab-content-institutional').children().map(function(){ return this.id }).toArray();

		if ( $('#tab-panel-institutional').children().length < 3 && ids.indexOf('pane-'+data.name) == -1) {

			if ($('#tab-panel-institutional').children().length == 0) {
				firstTabInst();
			}

			if ( $('#tab-panel-institutional').children().length == 3) {
				$('#tab-panel-institutional :last-child').delete();
				$('#tab-content-institutional :last-child').delete();
			}

			tab = $('<li>').attr({name: data.name, id: 'tab-'+data.name});
			link = $('<a>').attr({'data-toggle': 'tab', href: '#pane-'+data.name}).html(data.title);
			btnClose = $('<btn>').attr({'data-close': name, class: 'btn btn-primary btn-xs pull-right', onclick: "removeTabInst('"+data.name+"')"}).html('X');

			link.append(btnClose);
			tab.append(link);
			$('#tab-panel-institutional').append(tab);
			resizeTabs();

			tabPane = $('<div>').attr({id:'pane-'+data.name, class:'tab-pane fade'});

			if (data.href) {
				tabPaneContent = $('<iframe>').attr({width: '100%', height: '600px', src: data.href, scrolling: 'false'}).css({border: 'none'});
			}else if (data.img) {
				tabPaneContent = $('<div>').attr({class: 'img-container', scrolling: 'no'}).css({border: 0});
				image = $('<img>').attr('src', data.img).css({height: '100%', 'min-width': '100%'});
				tabPaneContent.append(image);
			}
			tabPane.append(tabPaneContent);
			$('#tab-content-institutional').append(tabPane);

			ids.push(data.name);

			$('#tab-panel-institutional '+'#tab-'+data.name+' a').tab('show');
			resizeTabsTabInst();
		}
	}

	function firstTabInst() {
		tab = $('<li>').attr({name: 'menu', id: 'tab-main-inst'});
		link = $('<a>').attr({'data-toggle': 'tab', href: '#pane-main-inst'}).html('Principal');

		tab.append(link);
		$('#tab-panel-institutional').append(tab);

		tabPane = $('<div>').attr({id:'pane-main-inst', class:'tab-pane fade'});
		tabPane.append($('<div>').html($('#content-institutional').html()));
		$('#content-institutional').html('');
		$('#tab-content-institutional').append(tabPane);

		$('#tab-panel-institutional').show();
		$('#tab-content-institutional').show();

		initializeLinksTabInst();
	}

	function removeTabInst(name) {
		$('#tab-'+name).remove();
		$('#pane-'+name).remove();		
		ids.splice(name, 1);
		if ( $('#tab-content-institutional').children().length == 1) {
			$('#content-institutional').html($('#pane-main-inst').html());
			$('#tab-main-inst').remove();
			$('#pane-main-inst').remove();
			$('#tab-panel-institutional').hide()
			$('#tab-content-institutional').hide()
			initializeLinksTabInst();
		}
		$('#tab-panel-institutional').last().find('a').tab('show');
	}

	function resizeTabsTabInst() {
		tabs = $('#tab-panel-institutional').children();
		tabs.css('width', calcPercentageNInst(tabs.length));
	}

	function calcPercentageNInst(number) {
		return ((1/number)*100)+'%';
	}

</script>

@endsection
