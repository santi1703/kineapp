<div class="noticia">
	@if(!is_null($resume))
	{{ $resume->description }}
	@endif
</div>

<div class="main-title">Noticias</div>

@foreach ($noticias as $noticia)
<div class="noticia">
	<div class="row">
		<div class="col-xs-12">
			<div>
				<div class="title">
					{{ $noticia->title }}
				</div>
				<div class="date">
					{{ $noticia->created_at->format('d/m/Y') }}
				</div>
			</div>
			<div class="lead">
				{{ $noticia->lead }}
			</div>
			@if (!empty($noticia->link))
			<div class="link">
				<a href="nav://{{ $noticia->link }}">{{ str_limit($noticia->link, 25) }}</a>
			</div>
			@endif
			@if ( !is_null($noticia->image()->first()) )			
			<div>
				<a href="nav://{{ $noticia->link }}">
					<div class="image" style="background-image: url('{{ asset('upload/img/'.$noticia->image()->first()->filename) }}');"></div>
				</a>
			</div>
			@endif
			<div class="description">
				{{ $noticia->description }}
			</div>
		</div>
	</div>
</div>
@endforeach
