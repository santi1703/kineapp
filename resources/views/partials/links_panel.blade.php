<div class="main-title">Vínculos de interés</div>

<ul class="nav nav-tabs" id="tab-panel-links" hidden>
	
</ul>

<div class="tab-content" id="tab-content-links" hidden>
	
</div>

<div id="content-links">
	<div class="list-group links-list" id="links-list">

		<a 
		href="nav://www.unc.edu.ar/vida-estudiantil/direcci%C3%B3n-de-deportes" 
		class="list-group-item open-tab ext-link"><img src="{{ asset('img/deportes.png') }}">Deportes</a>

		<a 
		href="nav://boletoeducativogratuito.cba.gov.ar/" 
		class="list-group-item open-tab ext-link"><img src="{{ asset('img/boleto-educativo.png') }}">BEG</a>

		<a 
		href="nav://comedor.unc.edu.ar/formulario/" 
		class="list-group-item open-tab ext-link"><img src="{{ asset('img/comedor.png') }}">Inscribirse en el comedor</a>

		<a 
		href="nav://www.unc.edu.ar/node/590" 
		class="list-group-item open-tab ext-link"><img src="{{ asset('img/menusemanal.png') }}">¿Qué comemos hoy?</a>
		
		<a 
		href="nav://www.unc.edu.ar/vida-estudiantil/pasos" 
		class="list-group-item open-tab ext-link"><img src="{{ asset('img/pasos.png') }}">Pasos</a>
		
		<a 
		href="#" 
		class="list-group-item open-tab" 
		name="link-acerca" 
		title="Acerca de..."
		data-elem="div"><img src="{{ asset('img/info.png') }}">Acerca de KineApp</a>


	</div>
</div>


@section('scripts')
@parent

<script>

	function initializeLinks () {
		$('#content-links .list-group-item').not('.ext-link').click( function(){ newTab($(this).attr('name'), $(this).attr('title'), $(this).attr('data-href')) });
	}

	initializeLinks();

	function newTab(name, title, href) {

		ids = $('#tab-content-links').children().map(function(){ return this.id }).toArray();

		if ( ids.indexOf('pane-'+name) == -1 ) {	
			
			if ( $('#tab-panel-links').children().length == 0 ) {
				firstTab();
			}
			initializeLinks();
			
			tab = $('<li>').attr({name: name, id: 'tab-'+name});
			link = $('<a>').attr({'data-toggle': 'tab', href: '#pane-'+name}).html(title);
			btnClose = $('<btn>').attr({'data-close': name, class: 'btn btn-primary btn-xs pull-right', onclick: "removeTab('"+name+"')"}).html('X');
			
			link.append(btnClose);
			tab.append(link);
			$('#tab-panel-links').append(tab);
			resizeTabs();
			
			tabPane = $('<div>').attr({id:'pane-'+name, class:'tab-pane fade'});

			tabPaneContent = $('<div>').css({'text-align':'center'}).html('<p>Kine App es una aplicaci&oacute;n para gestionar todos los aspectos relacionados con la vida estudiantil de los estudiantes de EKyF.</p><p>Distribuida por Estudiante x Estudiante</p><p>Desarrollada por <a href="nav://galileodevelops.com">Galileo Develops</a>.</p>');
			tabPane.append(tabPaneContent);
			$('#tab-content-links').append(tabPane);

			ids.push(name);

			$('#tab-panel-links '+'#tab-'+name+' a').tab('show');
		}
	}

	function firstTab() {
		tab = $('<li>').attr({name: 'menu', id: 'tab-main'});
		link = $('<a>').attr({'data-toggle': 'tab', href: '#pane-main'}).html('Principal');
		
		tab.append(link);
		$('#tab-panel-links').append(tab);

		tabPane = $('<div>').attr({id:'pane-main', class:'tab-pane fade'});
		tabPane.append($('<div>').html($('#content-links').html()));
		$('#content-links').html('');
		$('#tab-content-links').append(tabPane);

		$('#tab-panel-links').show()
		$('#tab-content-links').show()
	}

	function removeTab(name) {
		$('#tab-'+name).remove();
		$('#pane-'+name).remove();		
		ids.splice(name, 1);
		if ( $('#tab-content-links').children().length == 1) {
			$('#content-links').html($('#pane-main').html());
			$('#tab-main').remove();
			$('#pane-main').remove();
			$('#tab-panel-links').hide()
			$('#tab-content-links').hide()
			initializeLinks();
		}
		resizeTabs();
		$('#tab-panel-links').last().find('a').tab('show');
	}

	function resizeTabs() {
		tabs = $('#tab-panel-links').children();
		tabs.css('width', calcPercentageN(tabs.length));
	}

	function calcPercentageN(number) {
		return ((1/number)*100)+'%';
	}

</script>

@endsection
