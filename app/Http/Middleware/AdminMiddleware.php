<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( is_null($request->user())) {
            
            return redirect('/admin');
            
        }elseif ($request->user()->role_id != 1) {
            
            return redirect('/admin');
            
        }
        return $next($request);
    }
}
