<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Correlativity;
use App\Models\Image;
use View;
use Intervention\Image\ImageManagerStatic as IntImage;

class CorrelativitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $correlativities = Correlativity::all();

        return View::make('admin.correlativities.index', compact('correlativities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.correlativities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:correlativities|max:255',
            'image' => 'required|max:8192',
            ]);

        $data = $request->all();
        
        $imagen = IntImage::make($request->image);
        $rutaGuardado = public_path().'/upload/img/';
        $filename = str_random(32);
        $imagen->save($rutaGuardado.$filename);

        $imagenDB = Image::firstOrNew(['filename' => $filename]);
        
        $correlativity = Correlativity::create($data);

        $correlativity->image()->save($imagenDB);

        return redirect('admin/correlativities')->withMessage('Correlatividad creada correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $correlativity = Correlativity::find($id);

        return View::make('admin.correlativities.edit', compact('correlativity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $correlativity = Correlativity::find($id);

        $correlativity->fill($request->all());

        $correlativity->save();

        return redirect('admin/correlativities')->withMessage('Correlatividad actualizada correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Correlativity::find($id)->delete();

        $responseOk = 'Correlatividad eliminada.';
        if ( $request->ajax() ) {
            return json_encode(['code' => 'ok', 'message' => $responseOk]);
        }

        return redirect()->back()->withMessage($responseOk);
    }
}
