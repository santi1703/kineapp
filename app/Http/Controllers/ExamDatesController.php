<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExamDate;
use App\Models\Image;
use View;
use Validator;
use Intervention\Image\ImageManagerStatic as IntImage;

class ExamDatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $examDates = ExamDate::orderBy('position')->get();

        return View::make('admin.examDates.index', compact('examDates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.examDates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'name' => 'required|unique:exam_dates|max:255',
            'image' => 'required|max:8192',
            ]);
        
        $imagen = IntImage::make($request->image);
        $rutaGuardado = public_path().'/upload/img/';
        $filename = str_random(32);
        $imagen->save($rutaGuardado.$filename);

        $imagenDB = Image::firstOrNew(['filename' => $filename]);
        
        $examDate = ExamDate::create($data);

        $examDate->image()->save($imagenDB);

        return redirect('admin/examDates')->withMessage('Fecha de examen creada correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        ExamDate::find($id)->delete();

        $responseOk = 'Fecha de exámen eliminada.';
        if ( $request->ajax() ) {
            return json_encode(['code' => 'ok', 'message' => $responseOk]);
        }

        return redirect()->back()->withMessage($responseOk);
    }
}
