<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use App\Models\Image;
use View;
use Intervention\Image\ImageManagerStatic as IntImage;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticia::all();

        return View::make('admin.noticias.index', compact('noticias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.noticias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'max:8192',
            ]);

        $data = $request->all();

        $imagenDB = null;

        if ( !is_null($request->image) ) {
            $imagen = IntImage::make($request->image);
            $rutaGuardado = public_path().'/upload/img/';
            $filename = str_random(32);
            $imagen->save($rutaGuardado.$filename);
            
            $imagenDB = Image::firstOrNew(['filename' => $filename]);
        }
        
        $noticia = Noticia::create($data);

        if( !is_null($imagenDB) ) {
            $noticia->image()->save($imagenDB);
        }

        return redirect('admin/noticias')->withMessage('Noticia creada correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        Noticia::find($id)->delete();

        $responseOk = 'Noticia eliminada.';
        if ( $request->ajax() ) {
            return json_encode(['code' => 'ok', 'message' => $responseOk]);
        }

        return redirect()->back()->withMessage($responseOk);
    }
}
