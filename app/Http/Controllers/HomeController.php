<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use App\Models\Correlativity;
use App\Models\Schedule;
use App\Models\ExamDate;
use App\Models\Resume;
use View;

class HomeController extends Controller
{
    public function index() {

    	$noticias = Noticia::latest()->with('image')->take(25)->get();
    	$examDates = ExamDate::all();
    	$schedules = Schedule::orderBy('position')->get();
    	$correlativity = Correlativity::orderBy('position')->first();
    	$resume = Resume::latest()->first();

    	return View::make('index', compact('noticias', 'correlativity', 'examDates', 'schedules', 'resume'));
    }
}
