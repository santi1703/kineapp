<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\Image;
use View;
use Intervention\Image\ImageManagerStatic as IntImage;

class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::orderBy('position')->get();

        return View::make('admin.schedules.index', compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.schedules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:schedules|max:255',
            'image' => 'required|max:8192',
            ]);

        $data = $request->all();
        
        $imagen = IntImage::make($request->image);
        $rutaGuardado = public_path().'/upload/img/';
        $filename = str_random(32);
        $imagen->save($rutaGuardado.$filename);

        $imagenDB = Image::firstOrNew(['filename' => $filename]);
        
        $schedule = Schedule::create($data);

        $schedule->image()->save($imagenDB);

        return redirect('admin/schedules')->withMessage('Horario de cursado creado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule = Schedule::find($id);

        return View::make('admin.schedules.edit', compact('schedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $schedule = Schedule::find($id);

        $schedule->fill($request->all());

        $schedule->save();

        return redirect('admin/schedules')->withMessage('Horario de cursado actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Schedule::find($id)->delete();

        $responseOk = 'Horario de cursado eliminado.';
        if ( $request->ajax() ) {
            return json_encode(['code' => 'ok', 'message' => $responseOk]);
        }

        return redirect()->back()->withMessage($responseOk);
    }
}
