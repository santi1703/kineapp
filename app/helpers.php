<?php

function selectWithEmptyText( $array, $textEmpty = 'Seleccione uno' ) {
	$array[0] = $textEmpty;
	return $array;
}

function selectRangeFirstValue ( $maxValue, $textEmpty = 'Seleccione uno' ) {

	$array = ['' => $textEmpty];

	for ($i=1; $i <= $maxValue; $i++) { 
		$array[$i] = $i;
	}

	return $array;
}