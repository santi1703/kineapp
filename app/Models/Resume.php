<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    protected $fillable = ['description'];

    public static function boot() {
    	parent::boot();
    }
    
}
