<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamDate extends Model
{
    protected $fillable = ['name'];
    
    public static function boot() {
    	parent::boot();

    	static::deleting(function($model)
    	{
    		foreach ($model->image as $key => $image) {
    			$image->delete();
    		}
    	});
    }
    
	public function image() {
		return $this->morphMany('App\Models\Image', 'imageable');
	}
}
