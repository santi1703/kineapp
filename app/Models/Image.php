<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;

class Image extends Model
{	
	protected $fillable = ['imageable_id', 'imageable_type', 'filename'];

    public function imageable() {
    	return $this->MorphTo();
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($image)
        {
            unlink(public_path().'/upload/img/'.$image->filename);
        });
    }
}
