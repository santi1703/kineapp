<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
	protected $fillable = [
	'title', 'lead', 'description', 'link'
	];

    public static function boot() {
    	parent::boot();

    	static::deleting(function($model)
    	{
    		foreach ($model->image as $key => $image) {
    			$image->delete();
    		}
    	});
    }
    
	public function setLinkAttribute($value)
    {
        $this->attributes['link'] = str_replace(['http://', 'https://'], '' ,$value);
    }

	public function image() {
		return $this->morphMany('App\Models\Image', 'imageable');
	}
}
