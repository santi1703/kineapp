function initializeLinks () {
		$('#content-institutional #links-list').find(':not(".ext-link")').click( function(){ newTab($(this).attr('name'), $(this).attr('title'), $(this).attr('data-href')) });
	}

	initializeLinks();

	function newTab(name, title, href) {

		ids = $('#tab-content-links').children().map(function(){ return this.id }).toArray();

		if ( ids.indexOf('pane-'+name) == -1 ) {	

			if ( $('#tab-panel-links').children().length == 0 ) {
				firstTab();
			}
			initializeLinks();

			tab = $('<li>').attr({name: name, id: 'tab-'+name});
			link = $('<a>').attr({'data-toggle': 'tab', href: '#pane-'+name}).html(title);
			btnClose = $('<btn>').attr({'data-close': name, class: 'btn btn-primary btn-xs pull-right', onclick: "removeTab('"+name+"')"}).html('X');

			link.append(btnClose);
			tab.append(link);
			$('#tab-panel-links').append(tab);
			resizeTabs();

			tabPane = $('<div>').attr({id:'pane-'+name, class:'tab-pane fade'});

			tabPaneContent = $('<iframe>').attr({src:href, width: '100%', height: '600px', scrolling: 'no'}).css({border: 0});
			tabPane.append(tabPaneContent);
			$('#tab-content-links').append(tabPane);

			ids.push(name);

			$('#tab-panel-links '+'#tab-'+name+' a').tab('show');
		}
	}

	function firstTab() {
		tab = $('<li>').attr({name: 'menu', id: 'tab-main'});
		link = $('<a>').attr({'data-toggle': 'tab', href: '#pane-main'}).html('Principal');

		tab.append(link);
		$('#tab-panel-links').append(tab);

		tabPane = $('<div>').attr({id:'pane-main', class:'tab-pane fade'});
		tabPane.append($('<div>').html($('#content').html()));
		$('#content').html('');
		$('#tab-content-links').append(tabPane);

		$('#tab-panel-links').show()
		$('#tab-content-links').show()
	}

	function removeTab(name) {
		$('#tab-'+name).remove();
		$('#pane-'+name).remove();		
		ids.splice(name, 1);
		if ( $('#tab-content-links').children().length == 1) {
			$('#content').html($('#pane-main').html());
			$('#tab-main').remove();
			$('#pane-main').remove();
			$('#tab-panel-links').hide()
			$('#tab-content-links').hide()
			initializeLinks();
		}
		resizeTabs();
		$('#tab-panel-links').last().find('a').tab('show');
	}

	function resizeTabs() {
		tabs = $('#tab-panel-links').children();
		tabs.css('width', calcPercentageN(tabs.length));
	}

	function calcPercentageN(number) {
		return ((1/number)*100)+'%';
	}